-- Profile
INSERT INTO profile(firstname, lastname) VALUES ('Marco', 'Beckers');
INSERT INTO profile(firstname, lastname) VALUES ('Klaas', 'Deckers');

-- Goals
INSERT INTO goal (name, type, enddate, profile_id) VALUES ('Leg muscle 4k', 'Legs', '1922-02-02', 1);
INSERT INTO goal (name, type, enddate, profile_id ) VALUES ('Arms muscle 4k', 'Legs', '1982-02-02', 2);
--

--Exercises
INSERT INTO exercise(name, description, musclegroup, vidlink, imglink, repetitions,profile_id)
VALUES ('Bent-over rows',
        'Bent-over rows are a strength training exercise that primarily target the muscles of the upper back, including the rhomboids, middle trapezius, and latissimus dorsi.',
        'Back and Arms',
        'https://www.youtube.com/watch?v=7LaxVwRXqe4',
        'https://image.boxrox.com/2022/01/Bent-Over-row.jpg',
        15,
        1);

INSERT INTO exercise(name, description, musclegroup, vidlink, imglink, repetitions,profile_id)
VALUES ('Overhead press',
        'The overhead press, also known as the shoulder press, is a compound exercise that primarily targets the muscles of the shoulders, as well as the triceps and upper back.',
        'Arms',
        'https://www.youtube.com/watch?v=03p2g6mYunk',
        'https://i.insider.com/618e99f50e5c2f0019067ef7?width=1136&format=jpeg',
        20,
        2);

INSERT INTO exercise(name, description, musclegroup, vidlink, imglink, repetitions,profile_id)
VALUES ('Push-up',
        'Push-ups are a common bodyweight exercise that primarily target the chest, shoulders, and triceps muscles',
        'Arms',
        'https://www.youtube.com/watch?v=Eh00_rniF8E',
        'https://cdn.mos.cms.futurecdn.net/oYDbf5hQAePHEBNZTQMXRA.jpg',
        17,
        1);

INSERT INTO exercise(name, description, musclegroup, vidlink, imglink, repetitions,profile_id)
VALUES ('Squat',
        'Squats are a compound exercise that primarily target the muscles of the lower body, including the quadriceps, hamstrings, and glutes, as well as the core and back muscles.',
        'Legs',
        'https://www.youtube.com/watch?v=xqvCmoLULNY',
        'https://i0.wp.com/post.healthline.com/wp-content/uploads/2020/05/man-squatting-outdoors-1296x728-header.jpg?w=1155&h=1528',
        25,
        2);

INSERT INTO exercise(name, description, musclegroup, vidlink, imglink, repetitions,profile_id)
VALUES ('Deadlift',
        'Deadlifts are a compound exercise that primarily target the muscles of the lower back, glutes, hamstrings, and quads, as well as the core and grip muscles.',
        'Back, Arms',
        'https://www.youtube.com/watch?v=1ZXobu7JvvE',
        'https://mirafit.co.uk/wp/wp-content/uploads/2018/04/fitness-expert-performs-a-standard-deadlift-using-mirafit-gym-equipment-1024x683.jpg',
        17,
        1);

--Workouts
INSERT INTO workout(name, type, imglink, description_short, description_long,profile_id)
VALUES ('Upper body workout',
        'Arms, Shoulders',
        'https://i.ytimg.com/vi/alvaQZjxY_Y/maxresdefault.jpg',
        'Upper-body exercises build strength in the upper body by targeting muscle groups in the shoulders, arms, chest, abs, and back.',
        'Upper-body exercises build strength in the upper body by targeting muscle groups in the shoulders, arms, chest, abs, and back. Strong upper-body muscles can help improve your athletic performance, increase your stamina, and provide stability.Many upper-body workouts include bodyweight exercises that you can easily incorporate into your home workout routine.',
        1
       );

INSERT INTO workout(name, type, imglink, description_short, description_long,profile_id)
VALUES ('Lower body workout',
        'Legs, Back',
        'https://hips.hearstapps.com/hmg-prod/images/prevention-loop-051319-opener-1-1-1560270493.jpg',
        'Lower-body exercises are designed to build strength in the lower back, hips, glutes, and legs.',
        'Lower-body exercises are designed to build strength in the lower back, hips, glutes, and legs. Lower-body exercises include lunges, squats, deadlifts, and more. Exercising your lower body builds foundational strength for everyday movements. Leg exercises also help balance out your workout routine, helping you build muscle, stability, and speed in your lower body. They tend to target the hip flexors, quadriceps, hamstrings, and calf muscles. While everyone can benefit from lower-body workouts, they''re especially beneficial to runners, weightlifters, and swimmers.',
        2
        );

INSERT INTO workout(name, type, imglink, description_short, description_long,profile_id)
VALUES ('Full-body workout',
        'Arms, Shoulders, Legs, Back',
        'https://athleanx.com/wp-content/uploads/2022/12/FULL-BODY-WORKOUTS.jpg',
        'A full-body workout, also known as a total-body workout, is designed to target every major muscle group in a single workout session.',
        'A full-body workout could include compound exercises that target multiple muscle groups at once or a mix of isolation exercises that, combined, work your full body (a technique known as supersetting). In either case, a full-body workout routine includes a mix of strength training and cardio to effectively elevate your heart rate and build muscle throughout your entire body.',
        1
        );

--Programs
INSERT INTO program(name, type, imglink, description_short, description_long,profile_id)
VALUES ('Summerbody',
        'Full body',
        'https://miro.medium.com/v2/resize:fit:728/1*1qWqJdkFg7STzeC-hWo_Sw.jpeg',
        'Are you looking to perfect your beach body for the upcoming summer? If so, we have the right prescription just for you.',
        'It’s a full-body workout routine that utilizes a combination of high-intensity resistance training and cardio. Weighted resistance training is the best way to burn excess fat and calories compared to cardio alone. But if you want to maximize your gym time, we suggest you combine them to create a high-octane workout that transitions between weights and some form of cardio.',
        1
        );

INSERT INTO program(name, type, imglink, description_short, description_long,profile_id)
VALUES ('Superfit in a week',
        'Arms, Legs',
        'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/sporty-people-practicing-with-weights-royalty-free-image-531695662-1547208034.jpg',
        'Would you be superfit in a week then you should choose this program',
        'Let get your fit in a week time. In this program you get 2 difference workout with difference exercise through the week. Do this program and be fitter then you always been.',
        2
       );


--Goal-Workouts
INSERT INTO goal_workouts(goals_id, workouts_id)
VALUES (1,1);

INSERT INTO goal_workouts(goals_id, workouts_id)
VALUES (2,2);

--Goals-Programs
INSERT INTO goal_programs(goals_id, programs_id)
VALUES (1,2);
INSERT INTO goal_programs(goals_id, programs_id)
VALUES (2,1);

--Program_Workouts
INSERT INTO program_workouts(programs_id, workouts_id)
VALUES (1,3);

INSERT INTO program_workouts(programs_id, workouts_id)
VALUES (2,1);

INSERT INTO program_workouts(programs_id, workouts_id)
VALUES (2,2);

-- Workout_exercises
INSERT INTO workout_exercises(workouts_id, exercises_id)
VALUES (1,1);
INSERT INTO workout_exercises(workouts_id, exercises_id)
VALUES (1,2);
INSERT INTO workout_exercises(workouts_id, exercises_id)
VALUES (1,3);

INSERT INTO workout_exercises(workouts_id, exercises_id)
VALUES (2,4);
INSERT INTO workout_exercises(workouts_id, exercises_id)
VALUES (2,5);

INSERT INTO workout_exercises(workouts_id, exercises_id)
VALUES (3,1);
INSERT INTO workout_exercises(workouts_id, exercises_id)
VALUES (3,2);
INSERT INTO workout_exercises(workouts_id, exercises_id)
VALUES (3,3);
INSERT INTO workout_exercises(workouts_id, exercises_id)
VALUES (3,4);
INSERT INTO workout_exercises(workouts_id, exercises_id)
VALUES (3,5);