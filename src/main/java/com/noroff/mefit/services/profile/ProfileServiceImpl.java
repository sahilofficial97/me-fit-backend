package com.noroff.mefit.services.profile;

import com.noroff.mefit.models.Goal;
import com.noroff.mefit.models.Profile;
import com.noroff.mefit.repositories.GoalRepository;
import com.noroff.mefit.repositories.ProfileRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Service
public class ProfileServiceImpl implements ProfileService{
    private final ProfileRepository profileRepository;
    private final GoalRepository goalRepository;

    public ProfileServiceImpl(ProfileRepository profileRepository, GoalRepository goalRepository) {
        this.profileRepository = profileRepository;
        this.goalRepository = goalRepository;
    }

    @Override
    public Profile findById(Integer integer) {
        return profileRepository.findById(integer).get();
    }

    @Override
    public Collection<Profile> findAll() {
        return profileRepository.findAll();
    }

    @Override
    public Profile add(Profile entity) {
        return profileRepository.save(entity);
    }

    @Override
    public Profile update(Profile entity) {
        return profileRepository.save(entity);
    }

    @Override
    public void deleteById(Integer id) {
        profileRepository.deleteById(id);
    }

    @Override
    public void addUpdateGoals(int profileId, int[] goals) {
        Profile profile = profileRepository.findById(profileId).orElseThrow(() -> new EntityNotFoundException("could not find profile"));
        Set<Goal> goalList = new HashSet<>();

        for (int id : goals){
            goalList.add(goalRepository.findById(id).orElseThrow(() ->new EntityNotFoundException("could not find goals")));
        }
        profile.setGoals(goalList);
        profileRepository.save(profile);
    }

    @Override
    public void addUpdateCompletedGoals(int profileId, int[] completedGoals) {
        Profile profile = profileRepository.findById(profileId).orElseThrow(() -> new EntityNotFoundException("could not find profile"));
        profile.setCompletedGoals(completedGoals);
        profileRepository.save(profile);
    }

    @Override
    public Collection<Goal> getGoals(int profileId) {
        return profileRepository.findById(profileId).orElseThrow(()->new EntityNotFoundException("Profile not found")).getGoals();
    }

    @Override
    public Profile findProfileByFirstname(String name) {
        return profileRepository.findByFirstnameContaining(name);
    }

    @Override
    public Profile findByUserId(String id) {
        Profile profile=profileRepository.findByUserId(id);
        if (profile==null){
            throw new RuntimeException("this user does not exist");
        }
       return profile;
    }

}
