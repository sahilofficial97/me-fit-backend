package com.noroff.mefit.services.profile;

import com.noroff.mefit.models.Goal;
import com.noroff.mefit.models.Profile;
import com.noroff.mefit.services.CrudService;

import java.util.Collection;

public interface ProfileService extends CrudService<Profile, Integer> {
    void addUpdateGoals(int profileId, int[] goals);
    void addUpdateCompletedGoals(int profileId, int[] completedGoals );
    Collection<Goal> getGoals(int profileId);
    Profile findProfileByFirstname(String name);
    Profile findByUserId(String id);

}
