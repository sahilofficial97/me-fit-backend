package com.noroff.mefit.services.workout;

import com.noroff.mefit.models.Exercise;
import com.noroff.mefit.models.Program;
import com.noroff.mefit.models.Workout;
import com.noroff.mefit.repositories.ExerciseRepository;
import com.noroff.mefit.repositories.WorkoutRepository;
import com.noroff.mefit.services.exercise.ExerciseService;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;


@Service
public class WorkoutServiceImpl implements WorkoutService{

    @Autowired
    private final WorkoutRepository workoutRepository;
    @Autowired
    private final ExerciseRepository exerciseRepository;

    @Autowired
    private final ExerciseService exerciseService;

//    @Autowired
//    private final Logger loginfo;

    public WorkoutServiceImpl(WorkoutRepository workoutRepository, ExerciseRepository exerciseRepository, ExerciseService exerciseService) {
        this.workoutRepository = workoutRepository;
        this.exerciseRepository = exerciseRepository;
        this.exerciseService = exerciseService;
    }

    @Override
    public Workout findById(Integer id) {
        return workoutRepository.findById(id).orElseThrow(()->new EntityNotFoundException("Workout not found"));
    }

    @Override
    public Collection<Workout> findAll() {
        return workoutRepository.findAll();
    }

    @Override
    public Workout add(Workout entity) {
        return workoutRepository.save(entity);
    }

    @Override
    public Workout update(Workout entity) {
        return workoutRepository.save(entity);
    }

    @Override
    public void deleteById(Integer id) {
        Workout workout = workoutRepository.findById(id).orElseThrow(()->new EntityNotFoundException("Workout not found"));
        workout.getExercises().forEach(workouts -> workouts.setWorkouts(null));

        for (Program program : workout.getPrograms()) {
            program.getWorkouts().remove(workout);
        }

        workoutRepository.delete(workout);
    }

    @Override
    public void addUpdateExercises(int workoutId, int[] exercises) {
        Workout workout = workoutRepository.findById(workoutId).orElseThrow(() -> new EntityNotFoundException("could not find workout"));
        Set<Exercise> exerciseList = new HashSet<>();

        for (int id: exercises) {
            exerciseList.add(exerciseRepository.findById(id).orElseThrow(() ->new EntityNotFoundException("could not find exercises")));
        }

        workout.setExercises(exerciseList);
        workoutRepository.save(workout);
    }

    @Override
    public Collection<Exercise> getExercises(int workoutId) {
        return workoutRepository.findById(workoutId).orElseThrow(()->new EntityNotFoundException("Workout not found")).getExercises();
    }

    @Override
    public Workout findWorkoutByName(String name) {
        return workoutRepository.findByNameContaining(name);
    }
}
