package com.noroff.mefit.services.workout;

import com.noroff.mefit.models.Exercise;
import com.noroff.mefit.models.Workout;
import com.noroff.mefit.services.CrudService;
import org.hibernate.sql.ast.tree.expression.Collation;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface WorkoutService extends CrudService<Workout, Integer> {
    void addUpdateExercises(int workoutId, int[] exercises);

    Collection<Exercise> getExercises(int workoutId);
    Workout findWorkoutByName(String name);
}
