package com.noroff.mefit.services.goal;

import com.noroff.mefit.models.Exercise;
import com.noroff.mefit.models.Goal;
import com.noroff.mefit.models.Program;
import com.noroff.mefit.models.Workout;
import com.noroff.mefit.services.CrudService;
import org.hibernate.jdbc.Work;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;

public interface GoalService extends CrudService<Goal, Integer> {
    void addUpdatePrograms(int goalId, int[] programs);
    void addUpdateWorkouts(int goalId, int[] workouts);
    void addUpdateCompletedPrograms(int goalId, int[] completedPrograms );
    void addUpdateCompletedWorkouts(int goalId, int[] completedWorkouts);



    Collection<Program> getPrograms(int goalId);
    Collection<Workout> getWorkouts(int goalId);
    Collection<Workout> getWorkoutsFromProgram(int goalId, int programId);
    Collection<Exercise> getExercisesFromWorkouts(int goalId, int workoutId);
    Collection<Exercise> getExercisesFromProgramToWorkouts(int goalId, int programId, int workoutId);
    Goal findGoalByName(String name);
}
