package com.noroff.mefit.services.goal;

import com.noroff.mefit.models.Exercise;
import com.noroff.mefit.models.Goal;
import com.noroff.mefit.models.Program;
import com.noroff.mefit.models.Workout;
import com.noroff.mefit.repositories.GoalRepository;
import com.noroff.mefit.repositories.ProgramRepository;
import com.noroff.mefit.repositories.WorkoutRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class GoalServiceImpl implements GoalService {

    private final GoalRepository goalRepository;
    private final ProgramRepository programRepository;
    private final WorkoutRepository workoutRepository;


    public GoalServiceImpl(GoalRepository goalRepository, ProgramRepository programRepository, WorkoutRepository workoutRepository) {
        this.goalRepository = goalRepository;
        this.programRepository = programRepository;
        this.workoutRepository = workoutRepository;
    }

    @Override
    public Goal findById(Integer id) {
        return goalRepository.findById(id).get();
    }

    @Override
    public Collection<Goal> findAll() {
        return goalRepository.findAll();
    }

    @Override
    public Goal add(Goal entity) {
        return goalRepository.save(entity);
    }

    @Override
    public Goal update(Goal entity) {

        return goalRepository.save(entity);
    }

    @Override
    public void deleteById(Integer id) {
        Goal goal = goalRepository.findById(id).orElseThrow(()->new EntityNotFoundException("Goal not found"));
        goal.setProfile(null);
        goal.getWorkouts().forEach(goals-> goals.setGoals(null));
        goal.getPrograms().forEach(goals-> goals.setGoals(null));
        goalRepository.deleteById(id);
    }

    @Override
    public void addUpdatePrograms(int goalId, int[] programs) {
        Goal goal = goalRepository.findById(goalId).orElseThrow(() -> new EntityNotFoundException("could not find goal"));
        Set<Program> programList = new HashSet<>();

        for (int id : programs){
            programList.add(programRepository.findById(id).orElseThrow(() ->new EntityNotFoundException("could not find programs")));
        }
        goal.setPrograms(programList);
        goalRepository.save(goal);
    }

    @Override
    public void addUpdateWorkouts(int goalId, int[] workouts) {
        Goal goal = goalRepository.findById(goalId).orElseThrow(() -> new EntityNotFoundException("could not find goal"));
        Set<Workout> workoutList = new HashSet<>();

        for (int id : workouts){
            workoutList.add(workoutRepository.findById(id).orElseThrow(() ->new EntityNotFoundException("could not find workouts")));
        }
        goal.setWorkouts(workoutList);
        goalRepository.save(goal);
    }

    @Override
    public void addUpdateCompletedPrograms(int goalId, int[] completedPrograms) {
        Goal goal = goalRepository.findById(goalId).orElseThrow(() -> new EntityNotFoundException("could not find goal"));
        goal.setCompletedPrograms(completedPrograms);
        goalRepository.save(goal);
    }

    @Override
    public void addUpdateCompletedWorkouts(int goalId, int[] completedWorkouts) {
        Goal goal = goalRepository.findById(goalId).orElseThrow(() -> new EntityNotFoundException("could not find goal"));
        goal.setCompletedWorkouts(completedWorkouts);
        goalRepository.save(goal);
    }


    @Override
    public Collection<Program> getPrograms(int goalId) {
        return goalRepository.findById(goalId).orElseThrow(()->new EntityNotFoundException("Goal not found")).getPrograms();
    }

    @Override
    public Collection<Workout> getWorkouts(int goalId) {
        return goalRepository.findById(goalId).orElseThrow(()->new EntityNotFoundException("Goal not found")).getWorkouts();
    }

    @Override
    public Collection<Workout> getWorkoutsFromProgram(int goalId, int programId) {
        Goal goal = goalRepository.findById(goalId).orElseThrow(()->new EntityNotFoundException("Goal not found"));
        Program program = goal.getPrograms().stream()
                .filter(p -> p.getId() == programId )
                .findFirst()
                .orElseThrow(() -> new EntityNotFoundException("Program not found"));
        return program.getWorkouts();
    }

    @Override
    public Collection<Exercise> getExercisesFromWorkouts(int goalId, int workoutId) {
        Goal goal = goalRepository.findById(goalId).orElseThrow(()->new EntityNotFoundException("Goal not found"));
        Workout workout = goal.getWorkouts().stream()
                .filter(workouts -> workouts.getId() == workoutId )
                .findFirst()
                .orElse(null);
        if (workout == null){
            throw new EntityNotFoundException("Workout not found");
        }
        return workout.getExercises();
    }

    @Override
    public Collection<Exercise> getExercisesFromProgramToWorkouts(int goalId, int programId, int workoutId) {
        Goal goal = goalRepository.findById(goalId).orElseThrow(()->new EntityNotFoundException("Goal not found"));
        Program program = goal.getPrograms().stream()
                .filter(p -> p.getId() == programId)
                .findFirst()
                .orElseThrow(() -> new EntityNotFoundException("Program not found"));
        Workout workout = program.getWorkouts().stream()
                .filter(w -> w.getId() == workoutId)
                .findFirst()
                .orElseThrow(() -> new EntityNotFoundException("Workout not found"));
        return workout.getExercises();
    }

    @Override
    public Goal findGoalByName(String name) {
        return goalRepository.findByNameContaining(name);
    }


}
