package com.noroff.mefit.services.exercise;

import com.noroff.mefit.models.Exercise;
import com.noroff.mefit.models.Workout;
import com.noroff.mefit.repositories.ExerciseRepository;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class ExerciseServiceImpl implements ExerciseService{

    private final ExerciseRepository exerciseRepository;

    public ExerciseServiceImpl(ExerciseRepository exerciseRepository) {
        this.exerciseRepository = exerciseRepository;
    }

    @Override
    public Exercise findById(Integer id) {
        return exerciseRepository.findById(id).orElseThrow(()->new EntityNotFoundException("Exercise not found"));
    }

    @Override
    public Collection<Exercise> findAll() {
        return exerciseRepository.findAll();
    }

    @Override
    public Exercise add(Exercise entity) {
        return exerciseRepository.save(entity);
    }

    @Override
    public Exercise update(Exercise entity) {
        return exerciseRepository.save(entity);
    }

    @Override
    public void deleteById(Integer id) {
        Exercise exercise = exerciseRepository.findById(id).orElseThrow(()->new EntityNotFoundException("Exercise not found"));
        for (Workout workout : exercise.getWorkouts()) {
            workout.getExercises().remove(exercise);
        }
        exerciseRepository.delete(exercise);
    }

    @Override
    public Exercise findExerciseByName(String name) {
        return exerciseRepository.findByNameContaining(name);
    }
}
