package com.noroff.mefit.services.program;

import com.noroff.mefit.models.Goal;
import com.noroff.mefit.models.Program;
import com.noroff.mefit.models.Workout;
import com.noroff.mefit.repositories.ProgramRepository;
import com.noroff.mefit.repositories.WorkoutRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Service
public class ProgramServiceImpl implements ProgramService{

    private final ProgramRepository programRepository;
    private final WorkoutRepository workoutRepository;

    public ProgramServiceImpl(ProgramRepository programRepository, WorkoutRepository workoutRepository) {
        this.programRepository = programRepository;
        this.workoutRepository = workoutRepository;
    }

    @Override
    public Program findById(Integer id) {
        return programRepository.findById(id).get();
    }

    @Override
    public Collection<Program> findAll() {
        return programRepository.findAll();
    }

    @Override
    public Program add(Program entity) {
        return programRepository.save(entity);
    }

    @Override
    public Program update(Program entity) {
        return programRepository.save(entity);
    }

    @Override
    public void deleteById(Integer id) {
        Program program = programRepository.findById(id).orElseThrow(()->new EntityNotFoundException("Program not found"));
        program.getWorkouts().forEach(programs -> programs.setPrograms(null));
        for (Goal goal : program.getGoals()) {
            goal.getPrograms().remove(program);
        }
        programRepository.deleteById(id);
    }

    @Override
    public void addUpdateWorkouts(int programId, int[] workouts) {
        Program program = programRepository.findById(programId).orElseThrow(() -> new EntityNotFoundException("could not find program"));
        Set<Workout> workoutList = new HashSet<>();

        for (int id : workouts){
            workoutList.add(workoutRepository.findById(id).orElseThrow(() ->new EntityNotFoundException("could not find workouts")));
        }
        program.setWorkouts(workoutList);
        programRepository.save(program);
    }

    @Override
    public Collection<Workout> getWorkouts(int programId) {
        return programRepository.findById(programId).orElseThrow(()->new EntityNotFoundException("Program not found")).getWorkouts();
    }

    @Override
    public Program findProgramByName(String name) {
        return programRepository.findByNameContaining(name);
    }
}
