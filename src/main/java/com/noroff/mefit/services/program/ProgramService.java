package com.noroff.mefit.services.program;

import com.noroff.mefit.models.Program;
import com.noroff.mefit.models.Workout;
import com.noroff.mefit.services.CrudService;
import org.hibernate.jdbc.Work;

import java.util.Collection;

public interface ProgramService extends CrudService<Program, Integer> {
    void addUpdateWorkouts(int programId, int[] workouts);
    Collection<Workout> getWorkouts(int programId);
    Program findProgramByName(String name);
}
