package com.noroff.mefit.mappers;

import com.noroff.mefit.models.DTO.profile.ProfileGetSimpleDTO;
import com.noroff.mefit.models.DTO.program.ProgramDTO;
import com.noroff.mefit.models.DTO.program.ProgramGetSimpleDTO;
import com.noroff.mefit.models.DTO.program.ProgramPostDTO;
import com.noroff.mefit.models.DTO.program.ProgramPutDTO;
import com.noroff.mefit.models.Program;
import com.noroff.mefit.models.Workout;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "Spring")
public interface ProgramMapper {
    @Mapping(target = "profile", source = "profile.id")
    @Mapping(target = "workoutsId", source = "workouts", qualifiedByName = "workoutsToWorkoutsId")
    ProgramDTO programToProgramDTO(Program program);

    Collection<ProgramDTO> programToProgramDTO(Collection<Program> program);

    @Mapping(target = "profile.id" , source = "profileId")
    Program programPostDTOToprogram(ProgramPostDTO programPostDTO);

    Program programPutDTOToprogram(ProgramPutDTO programPutDTO);

    ProfileGetSimpleDTO programToProgramGetSimpleDTO(Program program);

    Collection<ProgramGetSimpleDTO> programToProgramGetSimpleDTO(Collection<Program> programs);

    @Named(value = "workoutsToWorkoutsId")
    default Set<Integer> map(Set<Workout> value){
        if (value == null)
            return null;
        return value.stream().map(workout -> workout.getId()).collect(Collectors.toSet());
    }
}
