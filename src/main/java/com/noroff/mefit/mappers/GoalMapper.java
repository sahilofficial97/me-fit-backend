package com.noroff.mefit.mappers;

import com.noroff.mefit.models.DTO.goal.GoalDTO;
import com.noroff.mefit.models.DTO.goal.GoalGetSimpleDTO;
import com.noroff.mefit.models.DTO.goal.GoalPostDTO;

import com.noroff.mefit.models.DTO.goal.GoalPutDTO;

import com.noroff.mefit.models.Goal;
import com.noroff.mefit.models.Program;
import com.noroff.mefit.models.Workout;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "Spring")
public interface GoalMapper {

    @Mapping(target = "profile", source = "profile.id")
    @Mapping(target = "workoutsId" , source = "workouts" , qualifiedByName = "workoutToWorkoutId")
    @Mapping(target = "programsId", source = "programs" , qualifiedByName = "programToProgramId")
    GoalDTO goalToGoalDTO(Goal goal);

    Collection<GoalDTO> goalToGoalDTO(Collection<Goal> goal);


    @Mapping(target = "profile.id" , source = "profileId")
    Goal goalPostDTOTogoal(GoalPostDTO goalPostDTO);

    Goal goalPutDTOTogoal(GoalPutDTO goalPutDTO);

    GoalGetSimpleDTO goalToGoalGetSimpleDTO(Goal goal);
    Collection<GoalGetSimpleDTO> goalToGoalGetSimpleDTO(Collection<Goal> goals);

    @Named(value = "workoutToWorkoutId")
    default Set<Integer> workoutMap(Set<Workout> value){
        if (value == null)
            return null;
        return value.stream().map(
                workout -> workout.getId()).collect(
                        Collectors.toSet()
        );
    }

    @Named(value = "programToProgramId")
    default Set<Integer> programMap(Set<Program> value){
        if (value == null)
            return null;
        return value.stream().map(
                program -> program.getId()).collect(
                        Collectors.toSet()
        );
    }
}
