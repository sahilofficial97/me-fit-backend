package com.noroff.mefit.mappers;

import com.noroff.mefit.models.DTO.workout.WorkoutDTO;
import com.noroff.mefit.models.DTO.workout.WorkoutGetSimpleDTO;
import com.noroff.mefit.models.DTO.workout.WorkoutPostDTO;
import com.noroff.mefit.models.DTO.workout.WorkoutPutDTO;
import com.noroff.mefit.models.Exercise;
import com.noroff.mefit.models.Workout;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "Spring")
public interface WorkoutMapper {
    @Mapping(target = "profile", source = "profile.id")
    @Mapping(target = "exercisesId", source = "exercises", qualifiedByName = "exerciseToExerciseId")
    WorkoutDTO workoutToWorkoutDTO(Workout workout);

    Collection<WorkoutDTO> workoutToWorkoutDTO(Collection<Workout> workout);

    @Mapping(target = "profile.id" , source = "profileId")
    Workout workoutPostDTOToworkout(WorkoutPostDTO workoutPostDTO);

    Workout workoutPutDTOToworkout(WorkoutPutDTO workoutPutDTO);

    WorkoutGetSimpleDTO workoutToWorkoutGetSimpleDTO(Workout workout);

    Collection<WorkoutGetSimpleDTO> workoutToWorkoutGetSimpleDTO(Collection<Workout> workouts);

    @Named(value = "exerciseToExerciseId")
    default Set<Integer> map(Set<Exercise> value){
        if (value == null)
            return null;
        return value.stream().map(
                exercise -> exercise.getId()).collect(
                        Collectors.toSet()
        );
    }
}
