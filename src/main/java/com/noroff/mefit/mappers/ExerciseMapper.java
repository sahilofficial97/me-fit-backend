package com.noroff.mefit.mappers;
import com.noroff.mefit.models.DTO.exercise.ExerciseDTO;
import com.noroff.mefit.models.DTO.exercise.ExerciseGetSimpleDTO;
import com.noroff.mefit.models.DTO.exercise.ExercisePostDTO;
import com.noroff.mefit.models.DTO.exercise.ExercisePutDTO;
import com.noroff.mefit.models.Exercise;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.Collection;

@Mapper(componentModel = "Spring")
public interface ExerciseMapper {
    @Mapping(target = "profile", source = "profile.id")
    ExerciseDTO exerciseToExerciseDTO(Exercise exercise);
    Collection<ExerciseDTO> exerciseToExerciseDTO(Collection<Exercise> exercise);
    @Mapping(target = "profile.id" , source = "profileId")
    Exercise exercisePostDTOToexercise(ExercisePostDTO exercisePostDPO);
    Exercise exercisePutDTOToexercise(ExercisePutDTO exercisePutDPO);

    ExerciseGetSimpleDTO exerciseToExerciseGetSimpleDTO(Exercise exercise);
    Collection<ExerciseGetSimpleDTO> exerciseToExerciseGetSimpleDTO(Collection<Exercise> exercises);



}
