package com.noroff.mefit.mappers;

import com.noroff.mefit.models.DTO.profile.ProfileDTO;
import com.noroff.mefit.models.DTO.profile.ProfileGetSimpleDTO;
import com.noroff.mefit.models.DTO.profile.ProfilePostDTO;
import com.noroff.mefit.models.DTO.profile.ProfilePutDTO;
import com.noroff.mefit.models.Goal;
import com.noroff.mefit.models.Profile;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "Spring")
public interface ProfileMapper {

    @Mapping(target = "goalsId", source = "goals", qualifiedByName = "goalToGoalId")
    ProfileDTO profileToProfileDTO(Profile profile);

    Profile profilePostDTOToprofile(ProfilePostDTO profilePostDTO);
    Profile profilePutDTOToprofile(ProfilePutDTO profilePutDTO);

    ProfileGetSimpleDTO profileToProfileGetSimpleDTO(Profile profile);

     @Named(value = "goalToGoalId")
    default Set<Integer> map(Set<Goal> value){
        if (value == null)
            return null;
        return value.stream().map(goal -> goal.getId()).collect(Collectors.toSet());
    }
}
