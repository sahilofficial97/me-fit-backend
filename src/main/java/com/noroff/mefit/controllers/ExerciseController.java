package com.noroff.mefit.controllers;

import com.noroff.mefit.mappers.ExerciseMapper;
import com.noroff.mefit.models.DTO.exercise.ExercisePostDTO;
import com.noroff.mefit.models.DTO.exercise.ExercisePutDTO;
import com.noroff.mefit.models.Exercise;
import com.noroff.mefit.services.exercise.ExerciseService;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.parameters.P;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

@RestController
@RequestMapping(path = "/exercise")
public class ExerciseController {
    private final ExerciseService exerciseService;
    private final ExerciseMapper exerciseMapper;
    private final ModelMapper mapper;


    public ExerciseController(ExerciseService exerciseService, ExerciseMapper exerciseMapper) {
        this.exerciseService = exerciseService;
        this.exerciseMapper = exerciseMapper;
        mapper =new ModelMapper();
    }

    @GetMapping()
    public ResponseEntity findAll(){
        return ResponseEntity.ok(
                exerciseMapper.exerciseToExerciseDTO(
                        exerciseService.findAll()
                ));
    }

    @GetMapping("{id}")
    public ResponseEntity findById(@PathVariable int id){
        return ResponseEntity.ok(
                exerciseMapper.exerciseToExerciseDTO(
                        exerciseService.findById(id)
                ));
    }

    @GetMapping("name/{name}")
    public ResponseEntity findByName(@PathVariable String name){
        return ResponseEntity.ok(exerciseMapper.exerciseToExerciseGetSimpleDTO(exerciseService.findExerciseByName(name)));
    }

    @PostMapping()
    public ResponseEntity addNewExercise(@RequestBody ExercisePostDTO exercisePostDTO) throws URISyntaxException {
        Exercise exercise = exerciseService.add(exerciseMapper.exercisePostDTOToexercise(exercisePostDTO));
        URI location = new URI("exercise/" + exercise.getId());
        return ResponseEntity.created(location).build();
    }
    @PutMapping("{id}")
    public ResponseEntity updateExercise(@RequestBody ExercisePutDTO exercisePutDTO, @PathVariable int id){
        if (id != exercisePutDTO.getId())
            return ResponseEntity.badRequest().build();
        exerciseService.update(exerciseMapper.exercisePutDTOToexercise(exercisePutDTO));
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("{id}")
    public ResponseEntity deleteExerciseById(@PathVariable int id){
        exerciseService.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
