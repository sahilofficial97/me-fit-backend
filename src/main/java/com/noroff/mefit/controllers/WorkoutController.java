package com.noroff.mefit.controllers;

import com.noroff.mefit.mappers.ExerciseMapper;
import com.noroff.mefit.mappers.WorkoutMapper;
import com.noroff.mefit.models.DTO.workout.WorkoutPostDTO;
import com.noroff.mefit.models.DTO.workout.WorkoutPutDTO;
import com.noroff.mefit.models.Exercise;
import com.noroff.mefit.models.Workout;
import com.noroff.mefit.services.workout.WorkoutServiceImpl;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;

@RestController
@RequestMapping(path = "/workout")
public class WorkoutController {
    private final WorkoutServiceImpl workoutService;
    private final WorkoutMapper workoutMapper;
    private final ExerciseMapper exerciseMapper;

    public WorkoutController(WorkoutMapper workoutMapper, ExerciseMapper exerciseMapper, WorkoutServiceImpl workoutService) {
        this.workoutMapper = workoutMapper;
        this.exerciseMapper = exerciseMapper;
        this.workoutService = workoutService;
    }

    @GetMapping()
    public ResponseEntity findAll(){
        return ResponseEntity.ok(workoutMapper.workoutToWorkoutDTO(workoutService.findAll()));
    }

    @GetMapping("{id}")
    public ResponseEntity findById(@PathVariable int id){
        return ResponseEntity.ok(workoutMapper.workoutToWorkoutDTO(workoutService.findById(id)));
    }
    @GetMapping("name/{name}")
    public ResponseEntity findByName(@PathVariable String name){
        return ResponseEntity.ok(workoutMapper.workoutToWorkoutGetSimpleDTO(workoutService.findWorkoutByName(name)));
    }

    @PostMapping()
    public ResponseEntity addNewWorkout(@RequestBody WorkoutPostDTO workoutPostDTO) throws URISyntaxException {
        Workout workout = workoutService.add(workoutMapper.workoutPostDTOToworkout(workoutPostDTO));
        URI uri = new URI("workout/" + workout.getId());
        return ResponseEntity.created(uri).build();
    }
    @PutMapping("{id}")
    public ResponseEntity updateWorkout(@RequestBody WorkoutPutDTO workoutPutDTO, @PathVariable int id){
        if (id != workoutPutDTO.getId())
            return ResponseEntity.badRequest().build();
        workoutService.update(workoutMapper.workoutPutDTOToworkout(workoutPutDTO));
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("{id}")
    public ResponseEntity deleteWorkoutById(@PathVariable int id){
        workoutService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("{id}/exercises")
    public ResponseEntity getExercises(@PathVariable int id){
        Collection<Exercise> exercises = workoutService.getExercises(id);
        return ResponseEntity.ok(exerciseMapper.exerciseToExerciseGetSimpleDTO(exercises));
    }

    @PutMapping("{id}/exercises")
    public ResponseEntity addUpdateExercises(@PathVariable int id, @RequestBody int[] exerciseIds){
        workoutService.addUpdateExercises(id, exerciseIds);
        return ResponseEntity.noContent().build();
    }
}



