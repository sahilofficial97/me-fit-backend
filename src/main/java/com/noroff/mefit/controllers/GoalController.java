package com.noroff.mefit.controllers;

import com.noroff.mefit.mappers.ExerciseMapper;
import com.noroff.mefit.mappers.GoalMapper;
import com.noroff.mefit.mappers.ProgramMapper;
import com.noroff.mefit.mappers.WorkoutMapper;
import com.noroff.mefit.models.DTO.goal.*;
import com.noroff.mefit.models.Exercise;
import com.noroff.mefit.models.Goal;
import com.noroff.mefit.models.Program;
import com.noroff.mefit.models.Workout;
import com.noroff.mefit.services.goal.GoalServiceImpl;
import com.noroff.mefit.services.profile.ProfileService;
import com.noroff.mefit.services.program.ProgramService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;

@RestController
@RequestMapping(path = "/goal")
public class GoalController {
    private final GoalServiceImpl goalService;
    private final GoalMapper goalMapper;
    private final ProgramMapper programMapper;
    private final WorkoutMapper workoutMapper;
    private final ExerciseMapper exerciseMapper;

    public GoalController(GoalServiceImpl goalService, GoalMapper goalMapper, ProgramMapper programMapper1, WorkoutMapper workoutMapper, ExerciseMapper exerciseMapper) {
        this.goalService = goalService;
        this.goalMapper = goalMapper;
        this.programMapper = programMapper1;
        this.workoutMapper = workoutMapper;
        this.exerciseMapper = exerciseMapper;
    }

    @GetMapping
    public ResponseEntity findAll(){
        return ResponseEntity.ok(goalMapper.goalToGoalDTO(goalService.findAll())) ;
    }

    @GetMapping("{id}")
    public ResponseEntity findById(@PathVariable int id){
        return ResponseEntity.ok(goalMapper.goalToGoalDTO(goalService.findById(id)));
    }

    @GetMapping("/name/{name}")
    public ResponseEntity findByName(@PathVariable String name){
        return ResponseEntity.ok(goalMapper.goalToGoalGetSimpleDTO(goalService.findGoalByName(name)));
    }

    @PostMapping()
    public ResponseEntity addNewGoal (@RequestBody GoalPostDTO goalPostDTO) throws URISyntaxException {
        Goal goal = goalService.add( goalMapper.goalPostDTOTogoal(goalPostDTO));
        URI uri = new URI("goal/" + goal.getId());
        return ResponseEntity.created(uri).build();

    }

    @PutMapping("{id}")
    public ResponseEntity updateGoal(@RequestBody GoalPutDTO goalPutDTO, @PathVariable int id) {
        if (id != goalPutDTO.getId())
            return ResponseEntity.badRequest().build();
        goalService.update(goalMapper.goalPutDTOTogoal(goalPutDTO));
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("{id}")
    public ResponseEntity deleteProgramById(@PathVariable int id){
        goalService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("{id}/programs")
    public ResponseEntity getPrograms(@PathVariable int id){
        Collection<Program> programs = goalService.getPrograms(id);
        return ResponseEntity.ok(programMapper.programToProgramGetSimpleDTO(programs));
    }

    @GetMapping("{id}/workouts")
    public ResponseEntity getWorkouts(@PathVariable int id){
        Collection<Workout> workouts = goalService.getWorkouts(id);
        return ResponseEntity.ok(workoutMapper.workoutToWorkoutGetSimpleDTO(workouts));
    }

    @GetMapping("{goalId}/workouts/{workoutId}/exercises")
    public ResponseEntity getAllExercisesFromGoalToWorkout(@PathVariable("goalId") int goalId, @PathVariable("workoutId") int workoutId ){
        Collection<Exercise> exercises = goalService.getExercisesFromWorkouts(goalId,workoutId);
        return ResponseEntity.ok(exerciseMapper.exerciseToExerciseGetSimpleDTO(exercises));
    }

    @GetMapping("{goalId}/programs/{programId}/workouts")
    public ResponseEntity getAllWorkoutsFromGoalToPrograms(@PathVariable("goalId") int goalId,
                                                           @PathVariable("programId") int programId){
    Collection<Workout> workouts = goalService.getWorkoutsFromProgram(goalId,programId);
    return ResponseEntity.ok(workoutMapper.workoutToWorkoutGetSimpleDTO(workouts));

    }

    @GetMapping("{goalId}/programs/{programId}/workouts/{workoutId}/exercises")
    public ResponseEntity getAllExercisesFromGoalToProgramToWorkout(@PathVariable("goalId") int goalId,
                                                                    @PathVariable("programId") int programId,
                                                                    @PathVariable("workoutId") int workoutId ){
        Collection<Exercise> exercises = goalService.getExercisesFromProgramToWorkouts(goalId,programId,workoutId);
        return ResponseEntity.ok(exerciseMapper.exerciseToExerciseGetSimpleDTO(exercises));

    }

    @PutMapping("{id}/programs")
    public ResponseEntity addUpdatePrograms(@PathVariable int id, @RequestBody int[] programIds){
        goalService.addUpdatePrograms(id, programIds);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("{id}/completedprograms")
    public ResponseEntity addUpdatecompletedPrograms(@PathVariable int id, @RequestBody int[] completedProgramIds){
        goalService.addUpdateCompletedPrograms(id, completedProgramIds);
        return ResponseEntity.noContent().build();
    }

     @PutMapping("{id}/workouts")
    public ResponseEntity addUpdateWorkouts(@PathVariable int id, @RequestBody int[] workoutIds){
        goalService.addUpdateWorkouts(id, workoutIds);
         return ResponseEntity.noContent().build();
     }

     @PutMapping("{id}/completedworkouts")
    public ResponseEntity addUpdatecompletedWorkouts(@PathVariable int id, @RequestBody int[] completedWorkoutIds){
        goalService.addUpdateCompletedWorkouts(id, completedWorkoutIds);
         return ResponseEntity.noContent().build();
     }










}
