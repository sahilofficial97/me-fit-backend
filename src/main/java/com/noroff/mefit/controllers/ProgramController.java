package com.noroff.mefit.controllers;

import com.noroff.mefit.mappers.ProgramMapper;
import com.noroff.mefit.mappers.WorkoutMapper;
import com.noroff.mefit.models.DTO.program.ProgramPostDTO;
import com.noroff.mefit.models.DTO.program.ProgramPutDTO;
import com.noroff.mefit.models.Program;
import com.noroff.mefit.models.Workout;
import com.noroff.mefit.services.program.ProgramService;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;

@RestController
@RequestMapping(path = "/program")
public class ProgramController {
    private final ProgramService programService;

    private final ProgramMapper programMapper;
    private final WorkoutMapper workoutMapper;
    private final ModelMapper mapper;

    public ProgramController(ProgramService programService, ProgramMapper programMapper, WorkoutMapper workoutMapper) {
        this.programService = programService;
        this.programMapper = programMapper;
        this.workoutMapper = workoutMapper;
        mapper =new ModelMapper();
    }

    @GetMapping()
    public ResponseEntity findAll(){

        return ResponseEntity.ok(
                programMapper.programToProgramDTO(
                        programService.findAll()
                ));
    }

    @GetMapping("{id}")
    public ResponseEntity findById(@PathVariable int id){
        return ResponseEntity.ok(
                programMapper.programToProgramDTO(
                        programService.findById(id)
                ));
    }

    @GetMapping("name/{name}")
    public ResponseEntity findByName(@PathVariable String name){
        return ResponseEntity.ok(programMapper.programToProgramGetSimpleDTO(programService.findProgramByName(name)));
    }

    @PostMapping
    public ResponseEntity addNewProgram(@RequestBody ProgramPostDTO programPostDTO) throws URISyntaxException {
        Program program = programService.add(programMapper.programPostDTOToprogram(programPostDTO));
        URI uri = new URI("program/" + program.getId());
        return ResponseEntity.created(uri).build();
    }

    @PutMapping("{id}")
    public ResponseEntity updateProgram(@RequestBody ProgramPutDTO programPutDTO, @PathVariable int id) {
        if (id != programPutDTO.getId())
            return ResponseEntity.badRequest().build();
        programService.update(programMapper.programPutDTOToprogram(programPutDTO));
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("{id}")
    public ResponseEntity deleteProgramById(@PathVariable int id){
        programService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("{id}/workouts")
    public ResponseEntity getWorkouts(@PathVariable int id){
        Collection<Workout> workouts = programService.getWorkouts(id);
        return ResponseEntity.ok(workoutMapper.workoutToWorkoutGetSimpleDTO(workouts));
    }

    @PutMapping("{id}/workouts")
    public ResponseEntity addUpdateWorkouts(@PathVariable int id, @RequestBody int[] workoutIds){
        programService.addUpdateWorkouts(id,workoutIds);
        return ResponseEntity.noContent().build();
    }
}
