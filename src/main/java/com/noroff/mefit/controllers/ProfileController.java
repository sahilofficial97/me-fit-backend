package com.noroff.mefit.controllers;

import com.noroff.mefit.mappers.GoalMapper;
import com.noroff.mefit.mappers.ProfileMapper;
import com.noroff.mefit.models.DTO.profile.ProfilePostDTO;
import com.noroff.mefit.models.DTO.profile.ProfilePutDTO;
import com.noroff.mefit.models.Goal;
import com.noroff.mefit.models.Profile;
import com.noroff.mefit.services.profile.ProfileService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;

@RestController
@RequestMapping(path = "/profile")
public class ProfileController {

    private final ProfileService profileService;

    private final ProfileMapper profileMapper;
    private final GoalMapper goalMapper;

    public ProfileController(ProfileService profileService, ProfileMapper profileMapper, GoalMapper goalMapper) {
        this.profileService = profileService;
        this.profileMapper = profileMapper;
        this.goalMapper = goalMapper;
    }

    @GetMapping({"{id}"})
    public ResponseEntity findById(@PathVariable int id){
        return ResponseEntity.ok(profileMapper.profileToProfileDTO(profileService.findById(id)));
    }

    @GetMapping("/userid/{id}")
    public ResponseEntity findByUserId(@PathVariable String id){
        return ResponseEntity.ok(profileMapper.profileToProfileDTO(profileService.findByUserId(id)));
    }

    @GetMapping("name/{firstname}")
    public ResponseEntity findByFirstname(@PathVariable String firstname){
        return ResponseEntity.ok(profileMapper.profileToProfileGetSimpleDTO(profileService.findProfileByFirstname(firstname)));
    }

    @PostMapping()
    public ResponseEntity addNewProfile(@RequestBody ProfilePostDTO profilePostDTO) throws URISyntaxException {
        Profile profile = profileService.add(profileMapper.profilePostDTOToprofile(profilePostDTO));
        URI uri = new URI("profile/" + profile.getId());
        return ResponseEntity.created(uri).build();
    }

    @PutMapping("{id}")
    public ResponseEntity updateProfile(@RequestBody ProfilePutDTO profilePutDTO, @PathVariable int id){
        if (id != profilePutDTO.getId())
            return ResponseEntity.badRequest().build();
        profileService.update(profileMapper.profilePutDTOToprofile(profilePutDTO));
        return ResponseEntity.noContent().build();
    }

    @GetMapping("{id}/goals")
    public ResponseEntity getGoals(@PathVariable int id){
        Collection<Goal> goals = profileService.getGoals(id);
        return ResponseEntity.ok(goalMapper.goalToGoalGetSimpleDTO(goals));
    }

    @PutMapping("{id}/goals")
    public ResponseEntity addUpdateGoals(@PathVariable int id, @RequestBody int[] goalIds){
        profileService.addUpdateGoals(id, goalIds);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("{id}/completedgoals")
    public ResponseEntity addUpdateCompletedGoals(@PathVariable int id, @RequestBody int[] completedGoalIds){
        profileService.addUpdateCompletedGoals(id, completedGoalIds);
        return ResponseEntity.noContent().build();
    }


}
