package com.noroff.mefit.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
public class Workout {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private String type;
    private String imglink;
    private String description_short;
    @Column(columnDefinition = "TEXT")
    private String description_long;
    @Column(columnDefinition = "boolean default false")
    private Boolean completed = false;

    //RelationShips
    @ManyToOne
    @JoinColumn(name = "profile_id")
    private Profile profile;
    @ManyToMany(mappedBy = "workouts")
    private Set<Goal> goals;
    @ManyToMany(mappedBy = "workouts")
    private Set<Program> programs;
    @ManyToMany()
    private Set<Exercise> exercises;
}
