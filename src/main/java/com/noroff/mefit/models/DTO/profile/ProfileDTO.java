package com.noroff.mefit.models.DTO.profile;

import com.noroff.mefit.models.Goal;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;
@Getter
@Setter
public class ProfileDTO {
    private int id;

    private String userId;
    private String firstname;
    private String lastname;
    private int[] completedGoals;
    private Set<Integer> goalsId;
}
