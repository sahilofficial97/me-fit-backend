package com.noroff.mefit.models.DTO.exercise;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExercisePostDTO {
    private String name;
    private String description;
    private String musclegroup;
    private String vidlink;
    private String imglink;
    private Integer repetitions;
    private Integer profileId;
}
