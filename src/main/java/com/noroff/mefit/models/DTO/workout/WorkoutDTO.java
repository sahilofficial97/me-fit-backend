package com.noroff.mefit.models.DTO.workout;

import com.noroff.mefit.models.Exercise;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Set;

@Getter
@Setter
public class WorkoutDTO {
    private int id;
    private String name;
    private String type;
    private String imglink;
    private String description_short;
    private String description_long;
    private int profile;
    private Set<Integer> exercisesId;
}
