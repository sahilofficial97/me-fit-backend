package com.noroff.mefit.models.DTO.profile;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class ProfilePostDTO {
    private String firstname;

    private String userId;

    private String lastname;
}
