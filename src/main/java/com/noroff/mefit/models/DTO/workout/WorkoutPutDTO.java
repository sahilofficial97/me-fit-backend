package com.noroff.mefit.models.DTO.workout;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WorkoutPutDTO {
    private int id;
    private String name;
    private String type;
    private String imglink;
    private String description_short;
    private String description_long;
}
