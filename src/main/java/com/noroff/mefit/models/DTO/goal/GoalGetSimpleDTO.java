package com.noroff.mefit.models.DTO.goal;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GoalGetSimpleDTO {
    private int id;
}
