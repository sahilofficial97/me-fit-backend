package com.noroff.mefit.models.DTO.profile;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProfilePutDTO {
    private int id;
    private String firstname;
    private String lastname;
}
