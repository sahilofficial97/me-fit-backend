package com.noroff.mefit.models.DTO.workout;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WorkoutGetSimpleDTO {
    private int id;
}
