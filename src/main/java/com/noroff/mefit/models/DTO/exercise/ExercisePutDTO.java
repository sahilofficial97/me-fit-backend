package com.noroff.mefit.models.DTO.exercise;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExercisePutDTO {
    private int id;
    private String name;
    private String description;
    private String musclegroup;
    private String vidlink;
    private String imglink;
    private Integer repetitions;
}
