package com.noroff.mefit.models.DTO.program;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProgramGetSimpleDTO {
    private int id;
}
