package com.noroff.mefit.models.DTO.program;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProgramPutDTO {
    private int id;
    private String name;
    private String type;
    private String imglink;
    private String description_short;
    private String description_long;
}

