package com.noroff.mefit.models.DTO.goal;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Set;

@Getter
@Setter
public class GoalDTO {
    private int id;
    private String name;
    private String type;
    private LocalDate enddate;
    private int[] completedWorkouts;
    private int[] completedPrograms;
    private int profile;
    private Set<Integer> workoutsId;
    private Set<Integer> programsId;


}
