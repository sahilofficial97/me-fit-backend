package com.noroff.mefit.models.DTO.profile;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProfileGetSimpleDTO {
    private int id;
}
