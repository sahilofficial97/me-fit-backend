package com.noroff.mefit.models.DTO.goal;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class GoalPutDTO {
    private int id;
    private String name;
    private String type;
    private LocalDate enddate;
}
