package com.noroff.mefit.models.DTO.program;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class ProgramDTO {
    private String id;
    private String name;
    private String type;
    private String imglink;
    private String description_short;
    private String description_long;
    private int profile;
    private Set<Integer> workoutsId;
}
