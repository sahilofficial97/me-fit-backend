package com.noroff.mefit.models.DTO.exercise;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExerciseGetSimpleDTO {
    private int id;
}
