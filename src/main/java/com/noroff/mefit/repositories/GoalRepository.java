package com.noroff.mefit.repositories;

import com.noroff.mefit.models.Goal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface GoalRepository extends JpaRepository<Goal,Integer> {
    Goal findByNameContaining(String name);


}
