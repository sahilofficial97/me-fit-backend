#FROM gradle:jdk17-alpine AS build
#RUN gradle bootJar
#
#
#
#FROM openjdk:17 AS runtime
#
#EXPOSE 8080
#
#ADD /build/libs/meFit-0.0.1-SNAPSHOT.jar .
#ENTRYPOINT ["java","-jar","meFit-0.0.1-SNAPSHOT.jar"]

FROM gradle:jdk17-alpine AS build
WORKDIR /app
COPY . .
RUN gradle bootJar



FROM openjdk:17 AS runtime
WORKDIR /app

EXPOSE 8080

ARG JAR_FILE=/app/build/libs/*.jar
COPY --from=build ${JAR_FILE} /app/app.jar
ENTRYPOINT ["java","-jar","app.jar"]

