# MeFit - A fitness tracker and resource library
MeFit is a back-end application built in Spring which motivates users to get in the best shape of their life! This is done through different methods:
- Goals: Users can set goals for themselves by choosing a set of fitness programs/workouts and track their progress.
- Library: Users that are new to fitness can learn everything they need to know in the library which details information for a variety exercises.
- Community: More experienced users can share their knowledge by contributing to the community and add new workouts / exercises.
- Achievements: Users unlock achievements and points as they accomplish their goals and are active contributors to the community

This Gitlab repository is only for the Back-end, if you are also interested in the Front-end visit: https://github.com/jimosine/me-fit-fe

## Usage

The api is deployed at the following address: https://me-fit-nl.azurewebsites.net


## Contributors
- Sahil Mankani @sahilofficial97
- Jim Buissink @jimosine
- Marco Beckers @MarcoB08 

## Build with
- Java(Gradle)
- Spring boot
- Postgress
- KeyCloak

